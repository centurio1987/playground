import { appendFile } from "fs"

interface BinaryTreeInf {
    sort(): any
    search(arg: any): any
    insert(arg: any): void
    delete(arg: any): void
}

enum Direction {
    left,
    right
}

export type NodeValueType = string | number
type SearchReturnValue<T extends NodeValueType> = {
    parent: undefined|null|Node<T>,
    current: undefined|null|Node<T>,
    direction: undefined|null|Direction
}

export class Node<T extends NodeValueType> {
    protected value: undefined|null|NodeValueType
    protected parent: undefined|null|Node<T>
    protected leftChild: undefined|null|Node<T>
    protected rightChild: undefined|null|Node<T>

    constructor(value: NodeValueType) {
        this.parent = null
        this.leftChild = null
        this.rightChild = null
        this.value = null
        if(isNodeValueType(value)){
            this.value = value
        } else {
            throw new TypeError()
        }
    }

    setValue(value: NodeValueType): Node<T> {
        this.value = value
        return this
    }

    setParent(parent: undefined|null|Node<T>): Node<T> {
        this.parent = parent
        return this
    }

    setLeftChild(child: undefined|null|Node<T>): Node<T> {
        this.leftChild = child
        return this
    }

    setRightChild(child: undefined|null|Node<T>): Node<T> {
        this.rightChild = child
        return this
    }
    get(): Node<T> {
        return this
    }
    getValue(): NodeValueType {
        if(isNodeValueType(this.value))
            return this.value
        else
            throw TypeError()
    }

    getLeftChild(): undefined|null|Node<T> {
        return this.leftChild
    }

    getRightChild(): undefined|null|Node<T> {
        return this.rightChild
    }

    getParent(): undefined|null|Node<T> {
        return this.parent
    }
}

export class BinaryTree implements BinaryTreeInf {
    protected node: Node<NodeValueType>
    constructor(arg: Node<NodeValueType> | any) {
        if(isNode(arg)){
            this.node = arg
        }
        else {
            this.node = new Node<NodeValueType>(arg)
        }
    }

    async sort(): Promise<any> {
        //TODO
    }

    search(arg: NodeValueType): SearchReturnValue<NodeValueType> {
        //TODO
        let parentValue = this.node.getParent()?.getValue()

        let data: SearchReturnValue<NodeValueType> = {
            parent: this.node.getParent(),
            current: this.node.get(),
            direction: parentValue && parentValue > this.node.getValue() ? Direction.left : Direction.right
        }
        while(data.current) {
            if(arg == data.current.getValue())
                return data
            else if(arg < data.current.getValue()) {
                data.parent = data.current
                data.current = data.current.getLeftChild()
                data.direction = Direction.left
                continue
            } else {
                data.parent = data.current
                data.current = data.current.getRightChild()
                data.direction = Direction.right
                continue
            }
        }
        return data
    }

    insert(arg: NodeValueType|Node<NodeValueType>): void {
        let val: NodeValueType
        if(isNode(arg))
            val = arg.getValue()
        else
            val = arg
        if(!this.node) {
            this.node = isNode(arg) ? arg : new Node<NodeValueType>(arg)
            return
        }

        var { parent, current, direction } = this.search(val)
        if (current)
            return
        else if(isNode(parent) && !current) {
            let newItem = isNode(arg) ? arg : new Node<NodeValueType>(arg)
            newItem.setParent(parent)
            if(direction === Direction.left) {
                parent?.setLeftChild(newItem)
            }
            else if (direction === Direction.right) {
                parent?.setRightChild(newItem)
            }
        }
    }

    delete(arg: NodeValueType|Node<NodeValueType>): void {
        let val: NodeValueType = isNode(arg) ? arg.getValue() : arg
        //존재 확인
        let { parent, current, direction } = this.search(val)
        if(!current)
            return

        if(isNode(parent)){
            //자식이 없을 경우
            if(!current.getLeftChild() && !current.getRightChild()){
                if(direction === Direction.left){
                    parent.setLeftChild(null)
                }
                else if(direction === Direction.right){
                    parent.setRightChild(null)
                }
            }
    
            //자식이 하나 있을 경우
            if(!current.getLeftChild() && current.getRightChild()){
                if(direction === Direction.left)
                    parent.setLeftChild(current.getRightChild())
                else if(direction === Direction.right)
                    parent.setRightChild(current.getRightChild())
            }
            else if(!current.getRightChild() && current.getLeftChild()){
                if(direction === Direction.left)
                    parent.setLeftChild(current.getLeftChild())
                else if(direction === Direction.right)
                    parent.setRightChild(current.getLeftChild())
            }
            //자식이 둘 있을 경우: 오른쪽 자식을 올린다.
            if(current.getLeftChild() && current.getRightChild()){
                let tempLeft: Node<NodeValueType> = current.getLeftChild() as Node<NodeValueType>
                parent.setRightChild(current.getRightChild())
                this.insert(tempLeft)
            }

        }
    }
}

//Type Guard

function isNode(arg: any): arg is Node<NodeValueType> {
    return arg instanceof Node
}

function isNodeValueType(arg: any): arg is NodeValueType {
    return typeof arg == "string" || typeof arg == "number"
}