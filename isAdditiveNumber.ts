function isAdditiveNumber(num: string) {
    let transNum = num.split('').map((x) => Number(x))
    if (transNum.length < 3) {
        return false
    } else if (transNum.length === 3) {
        return transNum[0] + transNum[1] === transNum[2]
    } else if (transNum.length === 4) {
        if ( transNum[0] + transNum[1] >= 10){
            return transNum[0] + transNum[1] === Number(transNum[2].toString() + transNum[3].toString())
        } else {
            return transNum[0] + transNum[1] === transNum[2] && transNum[1] + transNum[2] === transNum[3]
        }
    }
}

console.log(isAdditiveNumber("1"))
console.log(isAdditiveNumber("11")); // false
console.log(isAdditiveNumber("112")); // true
console.log(isAdditiveNumber("101")); // true
console.log(isAdditiveNumber("1123")); // true
console.log(isAdditiveNumber("1124")); // false
console.log(isAdditiveNumber("1011")); // true
console.log(isAdditiveNumber("9918")); // true
console.log(isAdditiveNumber("9917")); // false