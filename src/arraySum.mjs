import consola from "consola"

function arraySum(arr) {
    var total = 0
    for (var i of arr){
        total += i
    }
    return total
}

consola.info(arraySum([3,5]))
