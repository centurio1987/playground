/*hacker의 total score는 모든 챌린지에 대한 그 최대 스코어의 합이다. 
output column: hacker_id, name, total score of the hackers(aggr)
sort: score desc, hacker_id asc
where: total score가 0 초과인 결과만
*/

select h.hacker_id, h.name, sum(s.max_score) as total_score from hackers as h
inner join (select hacker_id, challenge_id, max(score) as max_score from submissions group by hacker_id, challenge_id) s on s.hacker_id = h.hacker_id
group by h.hacker_id, h.name
having total_score > 0
order by total_score desc, hacker_id asc;

