import { AStarGraph, VertexWithPoint } from '../src/graph'

test('validate AStarGraph', () => {
    var g: VertexWithPoint = new VertexWithPoint('g', [23,14,26,4,33])
    var d: VertexWithPoint = new VertexWithPoint('d', [35,23,32,43,54], [g])
    var e: VertexWithPoint = new VertexWithPoint('e', [13,24,35,87,95], [g])
    var f: VertexWithPoint = new VertexWithPoint('f', [54,68,23,87,12], [g])
    var c: VertexWithPoint = new VertexWithPoint('c', [13,24,23,5,34], [d])
    var b: VertexWithPoint = new VertexWithPoint('b', [2,6,2,13,24], [e, f])
    var a: VertexWithPoint = new VertexWithPoint('a', [1,4,3,5,3], [b,c])
    expect(a.getName()).toBe('a')
    expect(b).toBeInstanceOf(VertexWithPoint)

    var graph:AStarGraph = new AStarGraph(a, b, c, d, e, f, g)
    expect(graph).toBeInstanceOf(AStarGraph)
    expect(graph.euclidean(a.getPoint(), b.getPoint())).toBe(23)

    graph.calculate()
    var distanceTable = graph.getDistanceTable()
    expect(distanceTable.has('g')).toBeTruthy()
    expect(distanceTable.get('b')?.route).toStrictEqual(['a','b'])
    console.log(distanceTable.get('g')?.route)
})