import { BigInt2 } from "./plusMinus"

export class TypeGuardSet{
    static isString(val: any): val is string{
        return typeof val  == 'string'
    }
    static isNumberArr(val: any): val is number[]{
        return val.isArray() && val.every((x: any) => typeof x == 'number')
    }
    static isBigInt2(val: any): val is BigInt2{
        return val instanceof BigInt2
    }
}