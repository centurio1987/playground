export interface Heapable {
    getValue(): number
    get(): any
}

export class Heap {
    protected values: Heapable[]
    constructor(value?: Heapable) {
        this.values = []
        if (value) {
            this.values.push(value)
        }
    }

    push(value: Heapable): void {
        this.values.push(value)
        let parentIdx: number = Math.ceil((this.values.length - 1) / 2) - 1
        let subjectIdx: number = this.values.length - 1
        
        while( parentIdx !== -1 && this.values[parentIdx].getValue() > this.values[subjectIdx].getValue()) {
            let temp = this.values[parentIdx]
            this.values[parentIdx] = this.values[subjectIdx]
            this.values[subjectIdx] = temp
            subjectIdx = parentIdx
            parentIdx = Math.ceil(parentIdx / 2) - 1
        }
        
    }
    pop(): Heapable|undefined {
        let temp: Heapable = this.values[this.values.length - 1]
        this.values[this.values.length - 1] = this.values[0]
        this.values[0] = temp
        let root: Heapable|undefined = this.values.pop()
        
        let subjectIdx: number = 0
        let childBaseIdx: number = subjectIdx * 2

        while(this.values[subjectIdx]?.getValue() > this.values[childBaseIdx + 1]?.getValue() || this.values[subjectIdx]?.getValue() > this.values[childBaseIdx + 2]?.getValue()) {
            if(!this.values[childBaseIdx + 2]) {
                
            }
            if(!this.values[childBaseIdx + 2] || this.values[childBaseIdx + 1]?.getValue() < this.values[childBaseIdx + 2]?.getValue()){
                childBaseIdx = childBaseIdx + 1
                temp = this.values[childBaseIdx]
                this.values[childBaseIdx] = this.values[subjectIdx]
                this.values[subjectIdx] = temp
            } else if (this.values[childBaseIdx + 1].getValue() > this.values[childBaseIdx + 2].getValue()){
                childBaseIdx = childBaseIdx + 2
                temp = this.values[childBaseIdx]
                this.values[childBaseIdx] = this.values[subjectIdx]
                this.values[subjectIdx] = temp
            }

            subjectIdx = childBaseIdx
            childBaseIdx = childBaseIdx * 2
        }

        return root
    }

    getFromIndex(idx: number): number {
        return this.values[idx]?.getValue()
    }

    remove(target: Heapable): void{
        this.values = Array.from(this.values.filter((x, idx) => x.getValue() != target.getValue() ))
    }

    getWholeValues(): Heapable[] {
        return this.values
    }
    
    getSize(): number {
        return this.values.length
    }
}