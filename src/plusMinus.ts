import { TypeGuardSet } from './typeguard'

interface BigIntInterface<T>{
    plus(operand: string|number[]|T ): T
    subtract(operand: string|number[]|T ): T
    get(): string
    set(value: string|number[] ): void
    isMinus(): boolean
}

export class BigInt2 extends Number implements BigIntInterface<BigInt2>{
    protected value: number[]
    protected minus: boolean
    constructor(value: number[]|string , minus: boolean = false) {
        super()
        if(TypeGuardSet.isString(value)){
            if (value[0] === '-'){
                this.minus = true
                value = value.slice(1)
            } else {
                this.minus = false
            }
            this.value = value.split('').map((x) => Number(x))
        } else{
            this.value = value
            this.minus = minus
        }
    }
    plus(operand: string|number[]|BigInt2): BigInt2{
        //타입 변환
        if(!TypeGuardSet.isBigInt2(operand)){
            operand = new BigInt2(operand)
        }
        //값 역정렬
        let op1Rev: number[] = this.value
        let op2Rev: number[] = operand.getValueArray()
        op1Rev.reverse()
        op2Rev.reverse()
        //남은 자릿수 0으로 채우기
        if(op1Rev.length > op2Rev.length) {
            for ( let _ of Array.from({length: (op1Rev.length - op2Rev.length)}, (x,i) => i)){
                op2Rev.push(0)
            }
        } else if( op2Rev.length > op1Rev.length) {
            for ( let _ of Array.from({length: (op2Rev.length - op1Rev.length)}, (x,i) => i)){
                op1Rev.push(0)
            }
        }
        //둘 다 양수일 때의 알고리즘
        if(!this.minus && !operand.isMinus()){
            let mappedValue: [number, number][] = op1Rev.map((x,i) => [x, op2Rev[i]])
            let resultValue: number[] = []
            for (let v of mappedValue) {
                let sum = v[0] + v[1]
                let remainder = 0
                if (sum  + remainder >= 10) {
                    resultValue.unshift(sum + remainder - 10)
                    remainder = 1
                } else {
                    resultValue.unshift(sum + remainder)
                    remainder = 0
                }
            }
            return new BigInt2(resultValue, false)
        } else{
            //TODO//
            return new BigInt2('3333')
        }
    }
    subtract(operand: string|number[]|BigInt2): BigInt2{
        return new BigInt2('-3')
    }
    get(){
        return this.value.join('')
    }
    getValueArray(): number[]{
        return this.value
    }
    set(value: string|number[]){
        if(TypeGuardSet.isString(value)){
            this.value = value.split('').map((x) => Number(x) )
        } else {
            this.value = value
        }
    }

    isMinus(){
        return this.minus
    }
}