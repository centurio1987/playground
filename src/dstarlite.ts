import { Heap, Heapable } from './DataStructure/heap'
import * as _ from 'lodash'

abstract class Node {
    protected data?: any
    protected metaData?: any
    constructor(metaData?: any, data?: any) {
        if (data)
            this.data = data
        if (metaData)
            this.metaData = metaData
    }
}

export enum Flag {
    normal,
    start,
    goal,
    obstacle
}

abstract class MetaData {}

export class MetaDataForDStarLite extends MetaData{
    private g: number
    private rhs: number
    private flag: Flag
    private pos: Pos
    constructor(g: number, rhs: number, pos: Pos, flag: Flag = Flag.normal) {
        super()
        this.g = g
        this.rhs = rhs
        this.flag = flag
        this.pos = pos
    }
    setG(g: number): void {
        this.g = g
    }
    setRHS(rhs: number): void {
        this.rhs = rhs
    }
    setFlag(flag: Flag): void {
        this.flag = flag
    }
    setPos(pos: Pos): void {
        this.pos = pos
    }

    getG(): number {
        return this.g
    }
    getRHS(): number {
        return this.rhs
    }
    getFlag(): Flag {
        return this.flag
    }
    getPos(): Pos {
        return this.pos
    }
}

type Pos = [x: number, y: number]

export class NodeForDStarLite extends Node implements Heapable{

    constructor(metaData: MetaDataForDStarLite, data?: any) {
        super(metaData, data)
    }

    setFlag(flag: Flag): void {
        this.metaData.setFlag(flag)
    }
    setRHS(rhs: number): void {
        this.metaData.setRHS(rhs)
    }
    setG(g: number): void {
        this.metaData.setG(g)
    }
    setPos(pos: Pos): void {
        this.metaData.setPos(pos)
    }

    getG(): number {
        return this.metaData.getG()
    }
    getRHS(): number {
        return this.metaData.getRHS()
    }
    getFlag(): Flag {
        return this.metaData.getFlag()
    }
    getPos(): Pos {
        return this.metaData.getPos()
    }

    private calculateKey(): number {
        return Math.min(this.metaData.getG(), this.metaData.getRHS())
    }

    //For Heapable Interface
    getValue(): number {
        return this.calculateKey()
    }

    get(): NodeForDStarLite {
        return this
    }
}

interface calculatable  {
    calculate(): void
}

export class DStarLite implements calculatable {
    private mover: Pos
    private queue: Heap
    private field: NodeForDStarLite[][]
    private startPos: Pos
    private goalPos: Pos
    constructor(fieldSize: number = 6) {
        this.startPos = [5, 5]
        this.goalPos = [0, 0]
        this.field = []
        for (let i of _.range(fieldSize))
            this.field[i] = []
        this.mover = this.startPos
        this.queue = new Heap()
        this.fieldInit(fieldSize)
    }

    private fieldInit(fieldSize: number): void {
        //init to Infinite
        for (let i of _.range(fieldSize)) {
            for (let j of _.range(fieldSize)) {
                this.field[i][j] = new NodeForDStarLite(new MetaDataForDStarLite(Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY, [i, j], Flag.normal))
            }
        }
        //place obstables
        for (let z in _.range(fieldSize/2)) {
            this.field[_.random(1, fieldSize - 1)][_.random(1, fieldSize - 1)].setFlag(Flag.obstacle)
        }
        this.field[0][0].setFlag(Flag.goal)
        this.field[0][0].setRHS(0)
        this.queue.push(this.field[0][0])

        this.field[5][5].setFlag(Flag.start)
    }

    calculate(): void {
        this.computeShortestPath()
    }

    private computeShortestPath(): void {
        let startNode: NodeForDStarLite = this.field[this.startPos[0]][this.startPos[1]]
        while(startNode.getG() !== startNode.getRHS() || (this.queue.getFromIndex(0) !== undefined ? this.queue.getFromIndex(0) < startNode.getValue() : false)) {
            let u: NodeForDStarLite = this.queue.pop()?.get()
            console.log(u)
            if (u.getG() > u.getRHS()) {
                u.setG(u.getRHS())
            } else {
                console.log("else")
                u.setG(Number.POSITIVE_INFINITY)
                this.updateNode(u)
            }
            for (let i of _.range(u.getPos()[0] - 1, u.getPos()[0] + 2)) {
                if(i < 0  || i > this.field.length - 1)
                    continue
                for (let j of _.range(u.getPos()[1] - 1, u.getPos()[1] + 2)) {
                    if(j < 0 || j > this.field.length - 1 || this.field[i][j].getFlag() === Flag.obstacle || u === this.field[i][j])
                        continue
                    
                    this.updateNode(this.field[i][j])
                }
            }
        }
        console.log(startNode)
    }

    private updateNode(u: NodeForDStarLite): void {
        if(u.getFlag() !== Flag.goal) {
            let uPos: Pos = u.getPos()
            let rhs: number = Number.POSITIVE_INFINITY
            for (let i of _.range(uPos[0] - 1, uPos[1] + 2)) {
                if(i < 0 || i > this.field.length - 1)
                    continue
                for(let j of _.range(uPos[0] - 1, uPos[1] + 2)) {
                    if(j < 0 || j > this.field.length - 1 || this.field[i][j].getFlag() === Flag.obstacle || u === this.field[i][j])
                        continue
                    rhs = Math.min(rhs, this.calculateDistance(u, this.field[i][j]) + this.field[i][j].getG())
                }
            }
            u.setRHS(rhs)
        }
        this.queue.remove(u)
        if(u.getG() !== u.getRHS())
            this.queue.push(u)
    }

    private calculateDistance(a: NodeForDStarLite, b: NodeForDStarLite): number {
        return Math.abs(a.getPos().reduce((accr, cur) => accr += cur) - b.getPos().reduce((accr, cur) => accr += cur)) == 2? 1.4 : 1
    }

    addObstacle(): void {

    }

    deleteObstacle(): void {

    }
}