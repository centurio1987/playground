select h.hacker_id, h.name, count(c.challenge_id) as total_challenge from hackers as h
inner join challenges as c on h.hacker_id = c.hacker_id group by h.hacker_id, h.name
having count(*) = (select max(t1.count_c) from (select count(challenge_id) as count_c from challenges group by hacker_id) t1)
or total_challenge in (select t2.count_c from (select count(challenge_id) as count_c from challenges group by hacker_id) t2 group by count_c having count(count_c) = 1)
order by total_challenge desc, h.hacker_id asc;