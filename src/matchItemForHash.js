const consola = require('consola')

function matchItemForHash(arr, target){
    arrObj = {}
    for (let elem of arr){
        arrObj[`${elem}`] = elem
    }
    
    return arrObj[`${target}`]
}

module.exports = matchItemForHash

/***
 * 숫자 리스트와 타겟 값이 주어 졌을 때, 리스트에서 타겟 값과 일치하는 값을 찾는 서치 로직, 알고리즘은 해시매핑을 이용한다.
 * break down
 * - arr 리스트를 오브젝트로 만들기
 *      array.entries() => [0, 'a']
 * - 오브젝트화 된 arr를 target과 매치 시키기
 *      arr['1']
 * 
 * 
 * 자료 구조 및 테스트 값
 * target: Number
 * arr: Object
 *  arrObj = {
 *      1: 1,
 *      2: 2,
 *      3: 3      
 * } 
 */