import { time } from 'console'
import { Heap, Heapable } from './DataStructure/heap'

export type VertexAndDist<T extends Vertex> = {
    vertex: T,
    distance: number
}

interface Searchable {
    calculate(): any
}

/** VERTEX */
abstract class Vertex {
    protected name: string

    constructor(name: string) {
        this.name = name
    }

    getName(): string {
        return this.name
    }

    abstract getAdjacentvertices(): any
}

export class VertexForDist extends Vertex {
    protected adjacentvertices: VertexAndDist<VertexForDist>[]
    constructor(name: string, ...adjacentvertices: VertexAndDist<VertexForDist>[]){
        super(name)
        this.adjacentvertices = []
        if(adjacentvertices)
            this.adjacentvertices = adjacentvertices
    }

    getAdjacentvertices(): VertexAndDist<VertexForDist>[] {
        return this.adjacentvertices
    }
}

export class VertexWithPoint extends Vertex{
    protected point: number[]
    protected adjacentvertices: VertexWithPoint[]

    constructor(name: string, point: number[], adjacentvertices?: VertexWithPoint[]) {
        super(name)
        this.point = []
        this.point = point
        this.adjacentvertices = []
        if(adjacentvertices)
            this.adjacentvertices = adjacentvertices
    }

    getPoint(): number[] {
        return this.point
    }

    getAdjacentvertices(): VertexWithPoint[] {
        return this.adjacentvertices
    }
}

/** GRAPH */
abstract class Graph<T extends Vertex> {
    protected vertices: T[]
    protected distanceTable: Map<string, any>

    constructor(...vertices: T[]) {
        this.vertices = []
        this.distanceTable = new Map()
        if(vertices) {
            this.vertices.push(...vertices)
        }
    }

    abstract getDistanceTable(): Map<string, any>
}

abstract class DijkstraGraph extends Graph<VertexForDist> {
    protected distanceTable: Map<string, VertexAndDist<VertexForDist>>
    constructor(...vertices: VertexForDist[]) {
        super(...vertices)
        this.distanceTable = new Map()
        if(vertices){
            this.distanceTable.set(vertices[0].getName(), {vertex: vertices[0], distance: 0})
            for (let vertex of vertices.slice(1)) {
                this.distanceTable.set(vertex.getName(), {vertex: vertex, distance: Number.POSITIVE_INFINITY})
            }
        }
    }

    getDistanceTable(): Map<string, VertexAndDist<VertexForDist>> {
        return this.distanceTable
    }
}


export class DijkstraGraphWithHeap extends DijkstraGraph implements Searchable{
    private HeapableVertexAndDist = class implements Heapable {
        private item: VertexAndDist<VertexForDist>
        constructor(item: VertexAndDist<VertexForDist>) {
            this.item = item
        }
    
        getValue(): number {
            return this.item.distance
        }
    
        get(): VertexAndDist<VertexForDist>{
            return this.item
        }
    }

    constructor(...vertices: VertexForDist[]) {
        super(...vertices)
    }

    async calculate() {
        let heap: Heap = new Heap()
        heap.push(new this.HeapableVertexAndDist({vertex: this.vertices[0], distance: 0}))
        let adjvertices: VertexAndDist<VertexForDist>[] = this.vertices[0].getAdjacentvertices()
        
        for (let adjVertex of adjvertices) {
            if (adjVertex.distance < (this.distanceTable.get(adjVertex.vertex.getName())?.distance as number))
                this.distanceTable.set(adjVertex.vertex.getName(), adjVertex)
            heap.push(new this.HeapableVertexAndDist(adjVertex))
        }
        heap.pop()

        let next: Vertex
        while(heap.getSize() !== 0) {
            next = heap.pop()?.get().vertex
            adjvertices = next?.getAdjacentvertices()
            //인접버텍스 없는 경우
            if(!adjvertices)
                continue

            //거리 계산
            for (let item of adjvertices) {
                if((this.distanceTable.get(next.getName())?.distance as number) + item.distance < (this.distanceTable.get(item.vertex.getName())?.distance as number))
                    this.distanceTable.set(item.vertex.getName(), {vertex: item.vertex, distance: this.distanceTable.get(next.getName())?.distance as number + item.distance})
                heap.push(new this.HeapableVertexAndDist(this.distanceTable.get(item.vertex.getName()) as VertexAndDist<VertexForDist>))
            }
        }
    }
}

export class DijkstraGraphWithArray extends DijkstraGraph implements Searchable{

    constructor(...vertices: VertexForDist[]) {
        super(...vertices)
    }

    async calculate() {
        let notChecked: Set<string> = new Set()
        let checked: Set<string> = new Set()

        this.distanceTable.forEach((v, k, m) => notChecked.add(k))

        let adjvertices: VertexAndDist<VertexForDist>[] = this.vertices[0].getAdjacentvertices()
        
        for (let adjVertex of adjvertices) {
            adjVertex.vertex.getName()
            if (adjVertex.distance < (this.distanceTable.get(adjVertex.vertex.getName())?.distance as number))
                this.distanceTable.set(adjVertex.vertex.getName(), adjVertex)
        }

        notChecked.delete(this.vertices[0].getName())
        checked.add(this.vertices[0].getName())
        
        let next: Vertex
        while(notChecked.size) {
            next = this.getShortestVertex(notChecked)
            adjvertices = next.getAdjacentvertices()
            //인접버텍스 없는 경우
            if(!adjvertices){
                notChecked.delete(next.getName())
                checked.add(next.getName())
                continue
            }

            //거리 비교
            for (let item of adjvertices) {
                if((this.distanceTable.get(next.getName())?.distance as number + item.distance < (this.distanceTable.get(item.vertex.getName())?.distance as number)))
                    this.distanceTable.set(item.vertex.getName(), { vertex: item.vertex, distance: (this.distanceTable.get(next.getName())?.distance as number) + item.distance })
            }
            checked.add(next.getName())
            notChecked.delete(next.getName())
        }
    }

    getShortestVertex(notChecked: Set<string>): Vertex {
        let next = Array.from(this.distanceTable.values())?.filter((item) => notChecked.has(item.vertex.getName()))?.reduce((pre, cur) => {
            if (pre.distance < cur.distance)
                return pre
            else
                return cur
        }).vertex
        return next
    }
}

type RouteAndG = {
    route: string[],
    g: number
}


export class AStarGraph extends Graph<VertexWithPoint> implements Searchable {
    protected vertices: VertexWithPoint[]
    protected distanceTable: Map<string, RouteAndG>

    private HeapableNodeAndNumber = class implements Heapable {
        private value: [VertexWithPoint, number]
        constructor(value: [VertexWithPoint, number]) {
            this.value = value
        }
        get(): [VertexWithPoint, number] {
            return this.value
        }
        getValue(): number {
            return this.value[1]
        }
    }
    
    constructor(...vertices: VertexWithPoint[]) {
        super(...vertices)
        this.vertices = vertices
        this.distanceTable = new Map()
    }
    calculate() {
        var start: VertexWithPoint = this.vertices[0]
        var destination: VertexWithPoint = this.vertices[this.vertices.length - 1]
        var pQueue: Heap = new Heap()
        //시작 지점 F = G + H
        var G = 0
        var H = this.euclidean(start.getPoint(), destination.getPoint())
        var F = G + H
        this.distanceTable.set(start.getName(), {route: Array.from([start.getName()]), g: G})
        pQueue.push(new this.HeapableNodeAndNumber([start, F]))

        //루프 시작
        while(pQueue.getSize() !== 0 && !this.distanceTable.has(destination.getName())) {
            //TODO
            let vertex: VertexWithPoint = pQueue.pop()?.get()[0]
            let adjacentvertices: VertexWithPoint[] = vertex?.getAdjacentvertices()
            adjacentvertices?.map((adjVertex) => {
                console.timeLog("loop")
                G = this.distanceTable.get(vertex.getName())?.g as number + this.euclidean(vertex.getPoint(), adjVertex.getPoint())
                H = this.euclidean(adjVertex.getPoint(), destination.getPoint())
                F = G + H
                let route: string[] = Array.from((this.distanceTable.get(vertex.getName()) as RouteAndG).route)
                route.push(adjVertex.getName())
                this.distanceTable.set(adjVertex.getName(), {route: route, g: G})
                pQueue.push(new this.HeapableNodeAndNumber([adjVertex, F]))
            })
        }
    }
    euclidean(a: number[], b: number[]) {
        return Math.round(Math.sqrt(a.map((value, index) => Math.pow(value - b[index], 2)).reduce((sum, value) => sum + value)))
    }

    getDistanceTable(): Map<string, RouteAndG> {
        return this.distanceTable
    }
}