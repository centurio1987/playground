class Singleton {
    private static instance:Singleton

    static newInstance(): Singleton{
        if(!this.instance)
            this.instance = new Singleton()
        return this.instance
    }

    private constructor(){

    }

    hello(){
        console.log("hello")
    }
}

var singleton = Singleton.newInstance()
singleton.hello()
