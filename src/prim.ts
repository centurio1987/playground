interface Calculatable {
    calculate() : void
}

export class EuclidNorm{
    static l2norm(vec1: number[], vec2: number[]): number {
        return Math.sqrt(vec1.map((x, idx) => Math.pow(x - vec2[idx], 2)).reduce((accr, cur) => accr += cur))
    }
}

type Pos = [number, number]

export abstract class Vertex {
    protected pos: Pos
    protected next: Vertex|undefined
    constructor(pos: Pos) {
        this.pos = pos
        this.next = undefined
    }
    abstract getPos(): Pos
    abstract setNext(next: Vertex): void
    abstract getNext(): Vertex|undefined
}

export class VertexImpl extends Vertex {
    constructor(pos: Pos){
        super(pos)
    }
    getPos(): Pos {
        return this.pos
    }
    setNext(next: Vertex) {
        this.next = next
    }
    getNext(): Vertex|undefined{
        return this.next
    }
}

abstract class Prim implements Calculatable{
    protected vertices: Vertex[]|undefined
    protected primImpl: PrimInterface|undefined
    constructor() {
        this.vertices = undefined
        this.primImpl = undefined
    }
    calculate() {
        this.primImpl?.buildMST()
    }   
}

export interface PrimInterface{
    buildMST(...vertices: Vertex[]): Vertex[]
}

export class PrimImpl implements PrimInterface{
    buildMST(...vertices: Vertex[]): Vertex[] {
        //TODO
        
}

class MSTwithPrim extends Prim{
    constructor(...vertices: Vertex[]) {
        super()
        this.vertices = []
        this.vertices = vertices
        this.primImpl = new PrimImpl()
    }
}