import { mainModule } from "process"

async function add(a: number, b: number): Promise<number>{
    return a + b
}

add(2, 3).then(res => console.log(res))