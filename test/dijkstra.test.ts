import {VertexForDist, VertexAndDist, DijkstraGraphWithHeap, DijkstraGraphWithArray} from '../src/graph'

describe('Testing dijkstra algorithm', () => {
    test('validate VertexForDist', () => {
        let a: VertexForDist = new VertexForDist('a')
        let b: VertexForDist = new VertexForDist('b', {vertex: a, distance: 5})
        expect(a).toBeInstanceOf(VertexForDist)
        expect(a.getName()).toBe('a')
        expect(b.getAdjacentVertexes()[0].vertex.getName()).toBe('a')
    })
    test('validate Graph', async () => {
        let d: VertexForDist = new VertexForDist('d')
        let c: VertexForDist = new VertexForDist('c', {vertex: d, distance: 3})
        let b: VertexForDist = new VertexForDist('b', {vertex: c, distance: 5})
        let a: VertexForDist = new VertexForDist('a', {vertex: b, distance:4}, {vertex: c, distance:12}, {vertex: d, distance:10})
        let graph: DijkstraGraphWithHeap = new DijkstraGraphWithHeap(a, b, c, d)
        await graph.calculate()
        expect(graph.getDistanceTable().get('d')?.distance).toBe(10)
        expect(graph.getDistanceTable().get('c')?.distance).toBe(9)
        expect(graph.getDistanceTable().get('b')?.distance).toBe(4)
        expect(graph.getDistanceTable().get('a')?.distance).toBe(0)
    })
})