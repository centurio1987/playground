const consola = require('consola')

function matchItem(arr, target){
    let index = 0
    do {
        index = Math.floor(arr.length / 2)
        consola.log(index)
        if (target > arr[index])
            arr = arr.slice(index + 1)
        else if (target === arr[index])
            return arr[index]
        else
            arr = arr.slice(0, index)
    } while(index > 0)
    return "None"
}

module.exports = matchItem

/*
이진탐색
- 리스트 크기의 반이 되는 값을 비교 값의 인덱스로 삼는다.
- 타겟 값이 비교 값보다 크면 리스트의 오른쪽 부분만을 취하고(slice) 작으면 왼쪽 부분을 취한다.
- 인덱스 값이 1보다 작아지면 루프 종료
- example input
var arr = [2,36,54,12,94,43,25,74]
var target = 2
*/