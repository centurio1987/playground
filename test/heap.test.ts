import { Heap, Heapable } from '../src/DataStructure/heap'

class Temp implements Heapable {
    protected value: number
    constructor(value: number) {
        this.value = value
    }
    getValue(): number {
        return this.value
    }
    get(): number{
        return this.value
    }
}

test('validate heap', () => {
    let three: Heapable = new (class implements Heapable {
        private value: number
        constructor(value: number){
            this.value = value
        }
        getValue(): number{
            return this.value
        }
        get(): number{
            return this.value
        }
    })(3)
    let heap: Heap = new Heap(three)
    heap.push(new Temp(2))
    heap.push(new Temp(13))
    heap.push(new Temp(10))
    heap.push(new Temp(5))
    heap.push(new Temp(6))
    heap.push(new Temp(8))
    expect(heap.getWholeValues().map(x => x.getValue())).toStrictEqual([2,3,6,10,5,13,8])
    expect(heap.pop()?.getValue()).toBe(2)
    expect(heap.pop()?.getValue()).toBe(3)
    expect(heap.pop()?.getValue()).toBe(5)
})