function *testGenerator(item: any[]) {
    for (let i of item) {
        yield i
    }
}

let gen = testGenerator([1,2,3,4,5])
console.log(gen.next())
console.log(gen.next())

async function asyncAdd(a: number, b: number): Promise<number> {
    return await a + b
}

asyncAdd(34, 23).then((res) => {
    console.log(res)
}) 