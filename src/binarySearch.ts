interface Nodable {
    get(): any
    getValue(): number
}

export class SimpleNode implements Nodable{
    private value: number
    constructor(value:any){
        this.value = value
    }
    get(): number{
        return this.value
    }
    getValue(): number{
        return this.value
    }
}

export class BinarySearchTree {
    private tree: SimpleNode[]
    constructor(...nodes: SimpleNode[]) {
        if(nodes)
            this.tree = nodes
        else
            this.tree = []
    }

    insert(node: SimpleNode) {
        if(this.tree.length === 0){
            this.tree.push(node)
            return
        }

        const idx = this.get(node.getValue())
        this.tree[idx] = node
    }

    get(num: number): number{
        let idx = 0
        while(this.tree[idx]){
            if(num > this.tree[idx].getValue())
                idx = idx*2 + 2
            else if(num < this.tree[idx].getValue())
                idx = idx*2 + 1
            else
                return idx
        }
        return idx
    }

    getNode(num: number): SimpleNode {
        return this.tree[this.get(num)]
    }

    getTree(): SimpleNode[] {
        return this.tree
    }
}