function decorator(func: (...args: any) => any) {
    return (...args: any): any =>{
        console.log("start")
        let res: any = func(...args)
        console.log("end")
        return res
    }
}

function add(arg1: number, arg2: number) : void{
    console.log(arg1 + arg2)
}

decorator(add)(23, 43)
