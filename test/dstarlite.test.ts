import { DStarLite, NodeForDStarLite, MetaDataForDStarLite, Flag } from '../src/dstarlite'

describe("Validating D* lite algorithm", () => {
    test("validate algorithm", () => {
        let dstar: DStarLite = new DStarLite()
        expect(dstar).toBeInstanceOf(DStarLite)

        // if test, Set to private of calculateDistance
        // let aNode: NodeForDStarLite = new NodeForDStarLite(new MetaDataForDStarLite(5, 5, [3,3], Flag.normal))
        // let bNode: NodeForDStarLite = new NodeForDStarLite(new MetaDataForDStarLite(10, 10, [2,3], Flag.normal))
        // let cNode: NodeForDStarLite = new NodeForDStarLite(new MetaDataForDStarLite(5, 5, [2,2], Flag.normal))
        // expect(dstar.calculateDistance(aNode, bNode)).toBe(1)
        // expect(dstar.calculateDistance(aNode, cNode)).toBe(1.4)

        dstar.calculate()
    })
})