import path from 'path'

module.exports = {
    entry: "./src/plusMinus.ts",
    output: {
        path: path.resolve(__dirname, './dist')
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: [
                    { loader: "babel-loader", options: {
                        presets: ["@babel/preset-typescript", "@babel/preset-env"]
                    }},
                    { loader: "ts-loader"}
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', 'ts', 'js']
    }
}