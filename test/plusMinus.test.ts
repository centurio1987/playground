import { BigInt2 } from '../src/plusMinus'

describe("for plus minus class.", () => {
    test('is BigInt2 class exist?', () => {
        expect(new BigInt2([1,2,3,4,5,6])).toBeInstanceOf(BigInt2)
        expect(new BigInt2('123456').get()).toBe('123456')
        expect(new BigInt2([1,2,3,4,5,6]).get()).toBe('123456')
    })
    test('member variables get test.', () => {
        var bigint = new BigInt2('123456')
        expect(bigint.isMinus()).toBe(false)
    })
    test('plus and minus', () => {
        let plusNum1: BigInt2 = new BigInt2('23623422')
        let plusNum2: BigInt2 = new BigInt2('845552223333')
        let minusNum1: BigInt2 = new BigInt2('-666634553432344')
        expect(plusNum1.plus(plusNum2).get()).toBe('845575846755')
    })
})