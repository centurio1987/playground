# D* lite 설명
- [알고리즘 설명 자료](https://www.evernote.com/l/ADb0kdLNXyNAL6muqN5XRN9Z0QHm-O8CZC0/)
# break down
# 자료구조
- Field
  - 알고리즘이 수행 될 전체 공간
  - 2-Dimentional
  - [6][6]
# 디자인
- 구현 함수(calculate)를 스트레터지 패턴으로

# 목적함수
- (rhs)[https://bit.ly/3f8iLj0]
- (key)[https://bit.ly/3lHFp2R]
# 함수
- [X] 초기화
  - 우선순위 큐 초기화, 모든 노드의 rhs, g를 무한대로 초기화
  - 골의 rhs 0으로 초기화
  - 우선순위 큐에 골 등록
- [X] 최소 경로 찾기
  - [X] 버텍스 업데이트
  - [X] 키 계산
- [ ] 무브
  - 무버를 한 칸 움직인다.
  - goal이면 동작 안함
- [ ] 무브 올
  - 시작점부터 종점까지 움직인다.
- [X] 무버 초기화
- [ ] 장해물 추가
  - 랜덤 추가
  - 필드 변경 event emit
- [ ] 장해물 삭제
  - 랜덤 삭제
  - 필드 변경 event emit
- [ ] 경로 변경 핸들러
  - 코스트가 변경된 모든 엣지들에 대하여
  - 엣지 코스트 c(u, v)를 업데이트
  - 버텍스 를 업데이트
  - 마지막으로 최소 경로 찾기 함수를 재호출
# test case