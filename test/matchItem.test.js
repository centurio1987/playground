const matchItem = require('../src/matchItem')

test('', () => {
    expect(matchItem([2,36,54,12,94,43,25,74], 94)).toBe(94)
    expect(matchItem([2,36,54,12,94,43,25,74], 102)).toBe("None")
})