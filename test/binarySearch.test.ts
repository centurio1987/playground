import { BinarySearchTree, SimpleNode } from '../src/binarySearch'

test("validate binary search", () => {
    var bTree = new BinarySearchTree()
    bTree.insert(new SimpleNode(5))
    bTree.insert(new SimpleNode(52))
    bTree.insert(new SimpleNode(54))
    bTree.insert(new SimpleNode(12))
    bTree.insert(new SimpleNode(24))
    bTree.insert(new SimpleNode(42))
    bTree.insert(new SimpleNode(22))
    bTree.insert(new SimpleNode(12))
    bTree.insert(new SimpleNode(23))
    bTree.insert(new SimpleNode(43))
    expect(bTree.getNode(42)?.getValue()).toBe(42)
})