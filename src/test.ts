import * as _ from 'lodash'

let a: number[] = []
for (let i of _.range(10)) {
    a.push(_.random(0,100))
}
a = a.map((x) => {
    return Math.pow(x, 2)
})
console.log(a)